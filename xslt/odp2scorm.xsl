<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
  xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
  xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
  xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
  xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"
  xmlns:math="http://www.w3.org/1998/Math/MathML"
  xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
  xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
  xmlns:ooo="http://openoffice.org/2004/office"
  xmlns:ooow="http://openoffice.org/2004/writer"
  xmlns:oooc="http://openoffice.org/2004/calc"
  xmlns:dom="http://www.w3.org/2001/xml-events"
  xmlns:xforms="http://www.w3.org/2002/xforms"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0"
  xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0"
  xmlns:rpt="http://openoffice.org/2005/report"
  xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:grddl="http://www.w3.org/2003/g/data-view#"
  xmlns:officeooo="http://openoffice.org/2009/office"
  xmlns:tableooo="http://openoffice.org/2009/table"
  xmlns:drawooo="http://openoffice.org/2010/draw"
  xmlns:calcext="urn:org:documentfoundation:names:experimental:calc:xmlns:calcext:1.0"
  xmlns:loext="urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0"
  xmlns:field="urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0"
  xmlns:formx="urn:openoffice:names:experimental:ooxml-odf-interop:xmlns:form:1.0"
  xmlns:css3t="http://www.w3.org/TR/css3-text/"
  exclude-result-prefixes="office style text table draw fo xlink svg xhtml"
  version="1.0"
  >

  <xsl:output
    method="xml"
    version="1.0"
    encoding="utf-8"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    standalone="yes"
    indent="yes" />
  
  <xsl:variable name="cmpx" select="28" />
  <xsl:template match="office:body">
    <html>
      <head>
        <title>Translated by odp2scorm</title>
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      </head>
      <body>
        <xsl:apply-templates/>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="draw:frame/draw:text-box/text:p">
    <xsl:choose>
      <xsl:when test="../../@presentation:class='title' and node()">
        <h1>
          <xsl:apply-templates/>
        </h1>
      </xsl:when>
      <xsl:otherwise>
        <p><xsl:apply-templates/></p>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="table:table-row">
    <tr>
      <xsl:apply-templates/>
    </tr>
  </xsl:template>
  <xsl:template match="table:table-cell">
    <td><xsl:apply-templates/></td>
  </xsl:template>
  <xsl:template match="text:list">
    <ol>
      <xsl:apply-templates/>
    </ol>
  </xsl:template>
  <xsl:template match="text:list/text:list-item/text:p">
    <li><xsl:apply-templates/></li>
  </xsl:template>
  <xsl:template match="draw:page">
    <div class="page"><xsl:apply-templates/></div>
  </xsl:template>
  <xsl:template match="//draw:image">
    <img>
      <xsl:attribute name="src">
        <xsl:value-of select="substring(@xlink:href,1)" />
      </xsl:attribute>
      <xsl:attribute name="style">
        <xsl:value-of select="concat('position:absolute;', 'z-index: -1;', 'left: ', round(substring-before(../@svg:x,'cm')*$cmpx), 'px;', 'top: ', round(substring-before(../@svg:y,'cm')*$cmpx), 'px;')"/>
      </xsl:attribute>
      <xsl:attribute name="width">
        <xsl:value-of select="round(substring-before(../@svg:width,'cm')*$cmpx)" />
      </xsl:attribute>
      <xsl:attribute name="height">
        <xsl:value-of select="round(substring-before(../@svg:height,'cm')*$cmpx)" />
      </xsl:attribute>
      <xsl:attribute name="alt">
        <xsl:value-of select="@draw:name" />
      </xsl:attribute>
      
      <xsl:if test="svg:desc">
        <xsl:attribute name="longdesc">
          <xsl:value-of select="svg:desc" />
        </xsl:attribute>
      </xsl:if>
    </img>
  </xsl:template>

  <xsl:template match="//draw:text-box">
    <div>
      <xsl:attribute name="style">
        <xsl:value-of select="concat('position:absolute;', 'left: ', round(substring-before(../@svg:x,'cm')*$cmpx), 'px;', 'top: ', round(substring-before(../@svg:y,'cm')*$cmpx), 'px;', 'width: ', round(substring-before(../@svg:width,'cm')*$cmpx), 'px;', 'height: ', round(substring-before(../@svg:height,'cm')*$cmpx), 'px;')"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </div>
  </xsl:template>
  <xsl:template match="table:table">
    <table>
      <xsl:attribute name="style">
        <xsl:value-of select="concat('position:absolute;', 'left: ', round(substring-before(../@svg:x,'cm')*$cmpx), 'px;', 'top: ', round(substring-before(../@svg:y,'cm')*$cmpx), 'px;', 'width: ', round(substring-before(../@svg:width,'cm')*$cmpx), 'px;', 'height: ', round(substring-before(../@svg:height,'cm')*$cmpx), 'px;')"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </table>
  </xsl:template>
  
</xsl:stylesheet>
