import uno, unohelper, re
import traceback
from com.sun.star.awt import XActionListener
from com.sun.star.awt import XItemListener
from com.sun.star.text.ControlCharacter import PARAGRAPH_BREAK, LINE_BREAK
from com.sun.star.task import XJobExecutor

_qcm_pattern=re.compile(r"QCM_(\d+)")

def insertQCM(desktop, theme, sousTheme, num=None, lor=None):
    """
    Insère un QCM dans le document Writer courant ou dans un nouveau
    @param desktop une interface desktop de LibreOffice
    @param theme le thème choisi
    @param sousTheme le sous-thème choisi
    @param num un numéro pour la section. None par défaut, ce qui
    fait que le numéro sera ajusté pour être unique
    @param lor éventuellement une instance de LORunner, qui fournit,
    entre autres, une méthode mri(in: object) pour inspecter des objets
    de LibreOffice.
    """
    doc=desktop.getCurrentComponent()
    if not hasattr(doc, "Text"):
        doc = desktop.loadComponentFromURL( "private:factory/swriter","_blank", 0, () )
    text = doc.Text
    # va à la position actuelle du curseur visible.
    viewCursor=doc.getCurrentController().getViewCursor()
    cursor = text.createTextCursorByRange(viewCursor)

    sQCM = [s for s in doc.getTextSections() if _qcm_pattern.match(s.Name)]
    num=1
    for s in sQCM:
        n=int(_qcm_pattern.match(s.Name).group(1))
        if n >= num:
            num = n+1
    # à ce stade, num est un entier non nul plus grand que tous les
    # autres numéros de section

    # crée une section
    section=doc.createInstance( "com.sun.star.text.TextSection" )
    section.setName(f"QCM_{num:03d}")
    text.insertTextContent( cursor, section, 0 )

    # écrit dans la section
    sectionCursor=text.createTextCursorByRange(section.Anchor)

    text.insertString( sectionCursor, f"QCM_{num:03d}\n", 0 )

    # crée un tableau dans la section
    table = doc.createInstance( "com.sun.star.text.TextTable" )

    # de 2 lignes, 3 colonnes
    table.initialize( 2, 3 )
    text.insertTextContent( sectionCursor, table, 0 )
    table.setPropertyValue( "BackTransparent", uno.Bool(0) )
    table.setPropertyValue( "BackColor", 0xccccff )
    row = table.Rows.getByIndex(0)
    row.setPropertyValue( "BackTransparent", uno.Bool(0) )
    row.setPropertyValue( "BackColor", 0x666694 )

    table.getCellByName("A1").setString("Thème")
    table.getCellByName("B1").setString("Sous-Thème")
    table.getCellByName("C1").setString("Commentaires")
    table.getCellByName("A2").setString(theme)
    table.getCellByName("B2").setString(sousTheme)

    
    # une sous-section pour l'énoncé, une pour les questions,
    # une pour les métadonnées
    ssDic={
        'enonce': ["Écrire ici l'énoncé ; on peut insérer divers objets, dont des images\n"],
        'question': ["Faire ici une liste numérotée de 4 questions"],
        'meta': ["Métadonnées (syntaxe simple, à respecter)\nOK = 1, niveau = moyen\n"],
    }
    for ss, data in ssDic.items():
        sousSection=doc.createInstance( "com.sun.star.text.TextSection" )
        sousSection.setName(f"QCM_{ss}_{num:03d}")
        text.insertTextContent( sectionCursor, sousSection, 0 )
        ssCursor=text.createTextCursorByRange(sousSection.Anchor)
        text.insertControlCharacter(ssCursor, PARAGRAPH_BREAK, 0)
        text.insertString( ssCursor, data[0] , 0 )
        text.insertControlCharacter(ssCursor, PARAGRAPH_BREAK, 0)
        ssDic[ss].append(ssCursor) # save the cursor ... just in case
    return

programme_nsi={
    "Représentation des données : types et valeurs de base": [
        "Représentation binaire d’un entier relatif",
        "Représentation approximative des nombres réels : notion de nombre flottant",
        "Valeurs booléennes : 0, 1. Opérateurs booléens : and, or, not. Expressions booléennes",
        "Représentation d’un texte en machine. Exemples des encodages ASCII, ISO-8859-1, Unicode",
    ],
    "Représentation des données : types construits": [
        "P-uplets. p-uplets nommés",
        "Tableau indexé, tableau donné en compréhension",
        "Dictionnaires par clés et valeurs",
    ],
    "Traitement de données en tables": [
        "Indexation de tables",
        "Recherche dans une table",
        "Tri d’une table",
        "Fusion de tables",
    ],
    "Interactions entre l’homme et la machine sur le Web": [
        "Modalités de l’interaction entre l’homme et la machine",
        "Événements",
        "Interaction avec l’utilisateur dans une page Web",
        "Interaction client-serveur. Requêtes HTTP, réponses du serveur",
        "Formulaire d’une page Web",
    ],
    "Architectures matérielles et systèmes d’exploitation": [
        "Modèle d’architecture séquentielle (von Neumann)",
        "Transmission de données dans un réseau.",
        "Protocoles de communication. Architecture d’un réseau",
        "Systèmes d’exploitation",
        "Périphériques d’entrée et de sortie Interface Homme-Machine (IHM)",
    ],
    "Langages et programmation": [
        "Constructions élémentaires",
        "Diversité et unité des langages de programmation",
        "Spécification",
        "Mise au point de programmes",
        "Utilisation de bibliothèques",
    ],
    "Algorithmique": [
        "Parcours séquentiel d’un tableau",
        "Tris par insertion, par sélection",
        "Algorithme des k plus proches voisins",
        "Recherche dichotomique dans un tableau trié",
        "Algorithmes gloutons",
    ],
}

class ThemeDialog(unohelper.Base, XActionListener, XItemListener):
    def __init__(self, ctx, smgr, lor=None):
        self.ctx=ctx
        self.smgr=smgr
        self.lor=lor
        self.choixTheme=None
        self.choixSousTheme=None
        return

    def actionPerformed(self, actionEvent):
        """
        XActionListener event handler.
        """
        if actionEvent.Source == self.ok:
            if self.choixTheme and self.choixSousTheme:
                self.controlContainer.endExecute()
            else:
                self.labelControl.setText("OK seulement après choix d'un thème ET d'un sous-thème")
        elif actionEvent.Source == self.esc:
            self.choixTheme=None
            self.choixSousTheme=None
            self.controlContainer.endExecute()
        return
    
    def itemStateChanged(self, itemEvent):
        """XItemListener event handler."""
        pos = itemEvent.Source.SelectedItemPos
        if itemEvent.Source==self.theme:
            self.choixTheme=list(programme_nsi.keys())[pos]
            self.soustBoxModel.StringItemList = list(programme_nsi[self.choixTheme])
            self.choixSousTheme=None
        elif itemEvent.Source==self.sousTheme and self.choixTheme != None:
            self.choixSousTheme=list(programme_nsi[self.choixTheme])[pos]
        description = f"{self.choixTheme} : {self.choixSousTheme}"
        self.labelControl.setText(description)
        return

    def show(self):
        # create and execute a dialog
        dialogModel = self.smgr.createInstanceWithContext("com.sun.star.awt.UnoControlDialogModel", self.ctx)
        dialogModel.PositionX = 10
        dialogModel.PositionY = 10
        dialogModel.Width = 400
        dialogModel.Height = 120
        dialogModel.Title = "Choisir un thème et un sous-thème"

        # create listbox
        themeBoxModel = dialogModel.createInstance("com.sun.star.awt.UnoControlListBoxModel" )
        themeBoxModel.PositionX = 10
        themeBoxModel.PositionY = 5
        themeBoxModel.Width = 180
        themeBoxModel.Height = 80
        themeBoxModel.Name = "themeBox"
        themeBoxModel.StringItemList = list(programme_nsi.keys())

        # create listbox
        soustBoxModel = dialogModel.createInstance("com.sun.star.awt.UnoControlListBoxModel" )
        soustBoxModel.PositionX = 210
        soustBoxModel.PositionY = 5
        soustBoxModel.Width = 180
        soustBoxModel.Height = 80
        soustBoxModel.Name = "sousThemeBox"
        soustBoxModel.StringItemList = []
        self.soustBoxModel = soustBoxModel

        # create the OK button model and set the properties
        okButtonModel = dialogModel.createInstance("com.sun.star.awt.UnoControlButtonModel" )
        okButtonModel.PositionX = 320
        okButtonModel.PositionY  = 90
        okButtonModel.Width = 50
        okButtonModel.Height = 14
        okButtonModel.Name = "myOKbutton"
        okButtonModel.Label = "OK"

        # create the Escape button model and set the properties
        escButtonModel = dialogModel.createInstance("com.sun.star.awt.UnoControlButtonModel" )
        escButtonModel.PositionX = 250
        escButtonModel.PositionY  = 90
        escButtonModel.Width = 50
        escButtonModel.Height = 14
        escButtonModel.Name = "myESCbutton"
        escButtonModel.Label = "Esc"

        # create the label model and set the properties
        labelModel = dialogModel.createInstance( "com.sun.star.awt.UnoControlFixedTextModel" )
        labelModel.PositionX = 10
        labelModel.PositionY = 105
        labelModel.Width  = 350
        labelModel.Height = 14
        labelModel.Name = "myLabelName"
        labelModel.Label = "Choisir d'abord un thème ..."

        # insert the control models into the dialog model
        dialogModel.insertByName( "myOKbutton", okButtonModel)
        dialogModel.insertByName( "myESCbutton", escButtonModel)
        dialogModel.insertByName( "myLabelName", labelModel)
        dialogModel.insertByName( "themeBox", themeBoxModel)
        dialogModel.insertByName( "sousThemeBox", soustBoxModel)

        # create the dialog control and set the model
        controlContainer = self.smgr.createInstanceWithContext("com.sun.star.awt.UnoControlDialog", self.ctx)
        self.controlContainer = controlContainer
        controlContainer.setModel(dialogModel)

        oThemeBox = controlContainer.getControl("themeBox")
        self.theme = oThemeBox
        oSousThemeBox = controlContainer.getControl("sousThemeBox")
        self.sousTheme = oSousThemeBox
        oLabel = controlContainer.getControl("myLabelName")
        self.labelControl=oLabel
        oOkButton = controlContainer.getControl("myOKbutton")
        oEscButton = controlContainer.getControl("myESCbutton")
        self.ok = oOkButton
        self.esc = oEscButton
        
        # create a peer
        toolkit = self.smgr.createInstanceWithContext( "com.sun.star.awt.ExtToolkit", self.ctx)  

        controlContainer.setVisible(False)
        controlContainer.createPeer(toolkit, None)

        # bind the listeners
        oOkButton.addActionListener(self)
        oEscButton.addActionListener(self)
        oThemeBox.addItemListener(self)
        oSousThemeBox.addItemListener(self)

        # execute it
        controlContainer.execute()
        controlContainer.dispose()
        return

def addOneQCM(desktop=None, context=None, lor=None):
    """
    ajoute un QCM dans le document
    @param desktop None par défaut, sinon le desktop fourni par un LORunner
    @param context None par défaut, sinon le context fourni par un LORunner
    @param lor     None par défaut, sinon le LORunner
    """
    if desktop == None:
        desktop = XSCRIPTCONTEXT.getDesktop()
        context = XSCRIPTCONTEXT.getComponentContext()
    if desktop:
        smgr = context.getServiceManager()
        dialog = ThemeDialog(context, smgr)
        dialog.show()
        if dialog.choixTheme and dialog.choixSousTheme:
            insertQCM(desktop, dialog.choixTheme, dialog.choixSousTheme, lor=lor)
    return

g_exportedScripts = (addOneQCM,)

class QcmE3c(unohelper.Base, XJobExecutor):
    def __init__(self, ctx):
        self.ctx = ctx

    def trigger(self, args):
        try:
            desktop = self.ctx.ServiceManager.createInstanceWithContext(
                    'com.sun.star.frame.Desktop', self.ctx)
            addOneQCM(desktop, self.ctx)
        except:
            traceback.print_exc()


g_ImplementationHelper = unohelper.ImplementationHelper()

g_ImplementationHelper.addImplementation(
        QcmE3c,
        'org.example.QcmE3c',
        ('com.sun.star.task.Job',))
