# NSI-SCORM #

Ce projet permet de créer des QCM à quatre question

Il propose les fonctionnalités suivantes :

- une extension pour LibreOffice, qui permet de développer les QCM à l'aide du traitement de texte LibreOffice/writer
- un programme en Python3, qui tire d'un document ODT avec des QCM bien définis, un document HTML interactif ; une bibliothèque javascript, Revealjs est utilisée
- Ce même programme permet de fabriquer un paquet SCORM autonome. Celui-ci pèse un peu lourd, à cause de la bibliothèque revealJS

## Extension pour LibreOffice ##

On la trouve sous ``4QCM/4QCM_AddOn/4QCM.oxt``. Son rôle est de
faciliter la construction de QCM dans LibreOffice/Writer

### Installer l'extension ### 
Il suffit d'ouvrir le fichier ``4QCM.oxt`` dans LibreOffice.

Attention, il convient de régler les paramètres de sécurité, pour les
macros, à la valeur « moyenne » : une confirmation est demandée pour installer
de nouvelles extensions.

### Utilisation de l'extension ###
Un nouvel élément de menu est mis en place, on y accède par
Insertion -> QCM

Quand on active cet élément de menu, un dialogue permet de préciser le thème et le sous-thème du programme qui correspond à la nouvelle question, puis la structure se crée.

une section, qui en contient trois sous-sections :

- la section est nommée QCM_xxx (xxx est un nombre)
- les trois sous-sections sont nommées :
-   QCM_enonce_xxx, qui contiendra l'énoncé de la question
-   QCM_question_xxx qui sera remplie avec une liste numérotée de **quatre** propositions
-   QCM_meta_xxx qui contient des métadonnées, c'est à dire des données au sujet du QCM : le numéro de la bonne réponse, et le niveau : facile, moyen, difficile.

L'application LibreOffice/Writer permet de savoir dans quelle section se
trouve le curseur, il suffit de regarder le ligne de statut, en bas à droite.

### Résumé ###

Quand l'extension est installée, cliquer sur le menu Insertion->QCM, choisir
un thème et un sous-thème.

Dans la sous-section QCM_enonce_xxx, saisir l'énoncé de la question ;
on peut insérer des images. Si on veut placer une image, il peut être
judicieux de créer un tableau qui permette de situer l'image et le texte
dans des cases précises.

Dans la section QCM_question_xxx, créer une liste numérotée de quatre réponses; une seule doit être juste, les autres fausses. Pas de réponse du type « aucune des trois autres réponses ».

Dans la section QCM_meta_xxx, il faut et il suffit d'une phrase qui faite sur
le modèle ``OK = <numéro de 1 à 4>, niveau = <facile, moyen ou difficile>``
tout autre texte y est ignoré.

Tout ce qui peut être ajouté en dehors des sections prédéfinies sera
ignoré lors de la conversion vers un QCM.

## Programme convertisseur ##

Actuellement le convertisseur est un programme en Python3 assez rudimentaire,
qui fonctionne en ligne de commande. Il est disponible à 
``odt2qcm/odt2qcmhtml.py``

Pour le lancer, on invoque ``python3 odt2qcm/odt2qcmhtml.py <nom_de_fichier.odt>``

### dépendences ###

La version de Python3 doit être Python3.7 ou supérieure.

Il faut que l'emplacement ``/usr/share/javascript/revealjs`` contienne la
bibliothèque Javascript **Reveal.js**, disponible en téléchargement à
https://github.com/hakimel/reveal.js ... On peut aussi installer le paquet
debian non-officiel ``revealjs-lite_3.8.0-1_all.deb`` ici présent pour
disposer d'un sous-ensemble suffisant de cette bibliothèque.

### mode d'emploi ###

Lancer, dans un terminal, la commande ``python3 odt2qcmhtml.py <fichier>.odt``
Par exemple, si le fichier de traitement de texte qu'on a préparé se nomme
``monFichier.odt``, la commande sera ``python3 odt2qcmhtml.py monFichier.odt``

Si le fichier est correctement analysé, on voit défiler quelques messages
et à la fin, des données sont disponibles dans trois répertoires :

- le répertoire ``monFichier.odt.html/``, qui est un export au format HTML réalisé par LibreOffice lui-même ;
- le répertoire ``monFichier.odt.qcm/``, qui est une version HTML plus élaborée ; le fichier  ``monFichier.odt.qcm/monFichier.html`` est alors un questionnaire interactif autonome, vous pouvez le tester dans Firefox par exemple ;
- le répertoire ``monFichier.odt.scorm/``, qui contient un paquet SCORM, à l'emplacement ``monFichier.odt.scorm/monFichier.zip`` ; ce dernier fichier peut être utilisé dans des LMS (Learning Management Systems) comme Moodle.

## Un exemple d'utilisation ##

Vous pouvez aller dans le répertoire ``exemple`` et traiter le fichier
QCM.odt qui s'y trouve :

```bash
... $ cd exemple/
... exemple$ python3 ../odt2qcm/odt2qcmhtml.py QCM.odt
.
.
.
[quelques lignes de messages]
module scorm présent dans QCM.odt.scorm/QCM.zip
... exemple$
```
