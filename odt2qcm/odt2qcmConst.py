HEADER="""\
 <!doctype html>
<html lang="fr">
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>4QCM</title>
    <meta content="LibreOffice 6.1.5.2 (Linux)" name="generator">
    <meta content="2019-06-14T17:48:29.740277291" name="created">
    <meta content="2019-06-26T14:12:28.044094398" name="changed">
    <link rel="stylesheet" href="aux/revealjs/css/reset.css">
    <link rel="stylesheet" href="aux/revealjs/css/reveal.css">
    <link rel="stylesheet" href="aux/revealjs/css/theme/serif.css">

    <link rel="stylesheet" type="text/css" href="aux/style.css"> 

    <script src="aux/head.js"></script>
  </head>
  <body dir="ltr" lang="fr-FR">
    <div class="reveal">
      <div class="slides">
	<section>
	  <h2 id="nb-questions">QCM de n questions</h2>
	  <h3>Mode d'emploi :</h3>
	  <ul>
	    <li>Lire l'énoncé ;</li>
	    <li>Cliquer sur une des réponses (... ou pas) ;</li>
	    <li>On peut remettre à blanc les réponses ;</li>
	    <li>À la fin, on fait calculer le score.</li>
	  </ul>
	  <p>
	    3 points par bonne réponse, -1 points par réponse fausse,
	    rien en l'absence de réponse.
	  </p>
	</section>

"""

FOOTER="""\
	<section>
	  <div id="valider">
	    Évaluation du QCM
	    <div id="resultat"></div>
	  </div>
        </section>
      </div>
    </div>

    <script src="aux/revealjs/js/reveal.js"></script>

    <script src="aux/foot.js"></script>
    <!-- scormfunctions -->
  </body>
</html>
"""
