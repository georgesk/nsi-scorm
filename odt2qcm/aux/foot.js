// More info about config & dependencies:
// - https://github.com/hakimel/reveal.js#configuration
// - https://github.com/hakimel/reveal.js#dependencies
Reveal.initialize({
    width: 960,
    height: 700,
    margin: 0.03,
    minScale: 0.2,
    maxScale: 1.5,
    dependencies: [
	{ src: 'aux/revealjs/plugin/markdown/marked.js' },
	{ src: 'aux/revealjs/plugin/markdown/markdown.js' },
	{ src: 'aux/revealjs/plugin/notes/notes.js', async: true },
	{ src: 'aux/revealjs/plugin/highlight/highlight.js', async: true }
    ]
});
for(var c = 65; c < 69; c++){ // for A, B, C, D:
    Reveal.addKeyBinding( { keyCode: c, key: String.fromCharCode(c), description: 'Choose response '+ String.fromCharCode(c)}, function(code) {
	return function (){
	    response(code, Reveal.getCurrentSlide());
	}(c)
    } );
}
nbQuestions=document.querySelectorAll("ol.qcm-reponse").length;
document.querySelector("#nb-questions").innerHTML="QCM de "+nbQuestions+" questions"
Reveal.addEventListener( 'slidechanged', function( event ) {
    var valider=event.currentSlide.querySelector("#valider");
    if (valider && typeof(document.finished) == "undefined") {
        if ( ! confirm("Évaluer le travail ? (c'est irréversible)")) {
            Reveal.slide( 1 );
            return;
        } else {
            // On desactive les réponses et les boutons d'oubli
            reponses=document.querySelectorAll("div.qcm-reponse li");
            for(var i=0; i<reponses.length; i++){
                reponses[i].removeAttribute("onclick");
            }
            boutons=document.querySelectorAll("div.qcm-reponse button");
            for(var i=0; i<boutons.length; i++){
                boutons[i].classList.remove("visible");
                boutons[i].classList.add("hidden");
            }

	    // On calcule la note puis on l'affiche
	    Noter();
	    alert("Le score a été calculé : "+pageScore+"/100");

            // On inhibe les fonctions de rappel pour A, B, C, D
            document.finished=true;
        }
    }
} );

Noter(); // note pour zéro réponses choisies
