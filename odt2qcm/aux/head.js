// variables pour SCORM
var minCompletedTime; 
var pageScore;
var nbQuestions;

/**
 * fonction de rappel pour quand on clique sur une réponse : cela modifie
 * les attributs de l'élément, donc son apparence
 * @param elt l'élément "li" qui a été cliqué
 **/
function choixReponse(elt){
    var ol = elt.parentElement;
    var buttonId = ol.getAttribute("data-button-id");
    var items = ol.getElementsByTagName("li");
    var index=-1;
    for (var i=0; i < items.length; i++){
	var li = items[i];
	if (li==elt) index=i;
	li.classList.remove("undef");
	li.classList.remove("checked");
	li.classList.add("unchecked");
    }
    elt.classList.remove("undef");
    elt.classList.remove("unchecked");
    elt.classList.add("checked");
    // on rend le bouton qui suit "ol" visible
    var button=document.querySelector("button#"+buttonId);
    button.classList.remove("hidden");
    button.classList.add("visible");
    button.innerHTML="Oublier la réponse choisie ("+String.fromCharCode(65+index)+")";
}

/**
 * fonction de rappel pour Reveal si on tape A, B, C ou D
 * @param c un évènement clavier
 * @param slide le slide courant
 **/
function response(c, slide){
    // actif seulement si le document n'est pas fini !
    if (typeof(document.finished) != "undefined") return;
    var ol=slide.querySelector("ol");
    if(ol){
	var lines=ol.querySelectorAll("li");
	var li=lines[c.keyCode-65];
	choixReponse(li);
    }
}

/**
 * fonction de rappel pour oublier les réponses précédemment cliquées
 * @param elt l'élément "button" qui a été cliqué
 **/
function Oubli(elt){
    var id=elt.getAttribute("id");
    var ols = document.querySelectorAll("ol");
    var ol;
    for(var i=0; i<ols.length; i++){
	if (ols[i].getAttribute("data-button-id")==id){
	    ol=ols[i];
	}
    }
    var items=ol.getElementsByTagName("li");
    for (var i=0; i < items.length; i++){
	var li = items[i];
	li.classList.remove("checked");
	li.classList.remove("unchecked");
	li.classList.add("undef");
    }
    elt.classList.remove("visible");
    elt.classList.add("hidden");
}
/**
 * fonction de rappel pour évaluer le QCM
 * @param initial s'il existe permet de se passer du dialogue
 *        pour évaluer le travail.
 **/
function Noter(){
    // actif seulement si le document n'est pas fini !
    if (typeof(document.finished) != "undefined") return;

    var questions=document.querySelectorAll("ol");
    var meta=document.querySelectorAll("div.qcm-meta");
    re=/.*OK\s*=\s*(\d+)\s*,\s*.*/; // pour trouver la bonne réponse
    var score=0
    var bonnes = 0;
    var mauvaises = 0;
    var nd = questions.length;
    minCompletedTime = nd * 60 * 3; // temps conseillé 3 min par question
    var max=3*questions.length;
    for (var i=0; i < questions.length; i++) {
	var q=questions[i];
	var bonne=parseInt(meta[i].innerHTML.match(re)[1]); // GRRR
	var items=q.querySelectorAll("li");
	var choix=0; // indéfini
	for (var j=0; j < 4; j++){
	    var li=items[j];
	    if (li.classList.contains("checked")){
		choix=j+1;
		break;
	    }
	}
	if (choix>0) {                   // rien pour une réponse indéfinie
	    nd -= 1;
	    if (choix==bonne) {
		score +=3;              // trois points pour une bonne réponse
		bonnes +=1;
	    }
	    else {
		score-=1;               // moins un pour une mauvaise
		mauvaises +=1;
	    }
	}
    }
    pageScore = 100 * score / max;      // score sur 100 pour SCORM
    if (pageScore > 100) pageScore=100;
    if (pageScore < 0) pageScore=0;
    
    var texte = "<table style='font-size: 30px;'><tr><th>Réponses</th><th>comptes</th><th>score</th></tr>"
    texte += "<tr><td>Justes</td><td>"+bonnes+"</td><td>"+bonnes+"</td></tr>"
    texte += "<tr><td>Fausses</td><td>"+mauvaises+"</td><td>"+(-3*mauvaises)+"</td></tr>"
    texte += "<tr><td>N.R.</td><td>"+nd+"</td><td>0</td></tr>"
    texte += "<tr><td>Total</td><td>"+questions.length+"</td><td>"+score+"</td></tr>"
    var feedback=document.querySelector("div#resultat");
    feedback.innerHTML=texte;
    feedback.classList.add("visible");
    return pageScore;
}
