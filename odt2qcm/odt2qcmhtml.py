#! /usr/bin/python3

usage= """\
Conversion d'un fichier ODT vers du HTML stylé permettant de passer des
QCM pour E3C, et empaquetage dans une capsule SCORM. Une bibliothèque
Javascript "revealjs" est employée, pour un usage facile sur tous les
écrans.

Usage :
=======
{programme} <nom_du_fichier_source.odt>

Convertit le fichier source en QCM pertinent au format HTML, et produit
une capsule SCORM, qu'on peut utiliser dans Moodle.
"""

import subprocess, sys, os, re, random
from bs4 import BeautifulSoup
from odt2qcmConst import HEADER, FOOTER

def nettoieLignes(s):
    """
    Retire des sauts de lignes intempestifs d'un code HTML :
    les <br/> qui suivent un <p>, les <br/> précédant un </p> et
    les <p></p>
    @param s une instance de beautifulsoup ou une chaîne
    @return une chaîne de caractères, nettoyée
    """
    result=str(s)
    result=re.sub(r"<p>([\n\s]*<br/>)+", "<p>", result)
    result=re.sub(r"(<br/>[\n\s]*)+</p>", "</p>", result)
    result=re.sub(r"<p>[\n\s]*</p>", "", result)
    return result

def marqueLignesReponses(div, soup):
    """
    ajoute des attributs aux éléments li, par effet de bord ;
    ajoute un bouton pour oublier les réponses
    @param div une instance beautifulsoup de type <div>
    @param soup une instance racine de beautifulsoup
    """
    for li in div.find_all("li"):
        li["class"]="undef"
        li["onclick"]="choixReponse(this)"
        li["title"]="Cliquer ici pour sélectionner"
    ident=re.match(r"QCM_question_(\d+)", div["id"]).group(1)
    ol=div.find("ol")
    button=soup.new_tag("button")
    button["class"]   = "oubli hidden"
    button["id"]      = f"button{ident}"
    button["onclick"] = "Oubli(this)"
    button.string=f"""Oublier la réponse choisie"""
    ol.insert_after(button)
    ol["data-button-id"]=f"button{ident}"
    return
    

class QCM:
    n = 1
    def __init__(self,
                 soup, ident, theme, sousTheme, commentaire,
                 enonce, reponses, meta, melange=True):
        """
        Le constructeur
        @param soup une instance racine de beautifulsoup
        @param ident l'identifiant du QCM
        @param theme une chaîne
        @param sousTheme une chaîne
        @param commentaire une chaîne
        @param enonce un objet de type beautifulsoup
        @param reponses un objet de type beautifulsoup
        @param meta un objet de type beautifulsoup
        @param melange vrai par défaut ; s'il est vrai les réponses sont
          mélangées (mais la réponse juste est toujours connue)
        """
        self.error=0
        self.warning=0
        self._errmsg=[]
        self.soup=soup
        self.ident=ident
        self.theme=theme
        self.sousTheme=sousTheme
        self.commentaire=commentaire
        self.enonce=enonce
        self.reponses=reponses
        self.meta=meta
        self.ok=-1        # numéro de la bonne réponse
        self.niveau=None
        self.melange=melange
        self.valide()
        self.shuffle()
        return

    def __str__(self):
        return """[QCM : {}]""".format(self.__dict__)

    def shuffle(self):
        """
        Mélange les réponses, si self.melange est vrai. Maintient en
        synchronisation la liste des réponses et la valeur de self.ok.
        """
        if not self.melange: return
        nrep=self.reponses.find_all("li")
        bonne_reponse = nrep[self.ok-1]
        permutation=list(range(4))
        random.shuffle(permutation)
        nrep=[nrep[permutation[i]] for i in range(4)]
        self.ok = 1 + permutation.index(self.ok-1)
        self.reponses=BeautifulSoup(f"""<div class="qcm-reponse" dir="ltr" id="QCM_question_{QCM.n:03d}"><ol>{nrep[0]}{nrep[1]}{nrep[2]}{nrep[3]}</ol></div>""", "lxml").find("div")
        QCM.n+=1
        assert bonne_reponse == nrep[self.ok-1]

    def toReveal(self):
        """
        forme du code HTML pour une section de slide, avec la bibliothèque
        revealjs
        """
        marqueLignesReponses(self.reponses, self.soup)
        return """\
	<section>
{enonce}
{reponses}
<div class="qcm-meta">OK = {ok}, niveau = {niveau}</div>
	</section>
""".format(
    enonce=nettoieLignes(self.enonce),
    reponses=nettoieLignes(self.reponses),
    ok=self.ok,
    niveau=self.niveau,
)

    def addError(self, msg):
        self.error+=1
        self._errmsg.append(f"[QCM {self.ident}] Erreur " + msg)
        return

    def addWarning(self, msg):
        self.warning+=1
        self._errmsg.append(f"[QCM {self.ident}] Avertissement " + msg)
        return

    def valide(self):
        if self.theme==None:
            self.addError("dans le Thème : tableau manquant ?")
        if self.sousTheme==None:
            self.addError("dans le Sous-Thème : tableau manquant ?")
        if self.commentaire==None:
            self.addError("dans le Commentaire : tableau manquant ?")
        if self.enonce==None:
            self.addError("dans l'Énoncé : section d'énoncé manquante ?")
        if self.reponses==None:
            self.addError("dans les Réponses : section de réponses manquante ?")
        if self.meta==None:
            self.addError("dans les Métadonnées : section des métadonnées manquante ?")
        if self.enonce:
            # ajout d'une classe
            addClass(self.enonce, "qcm-enonce")
        if self.reponses:
            #ajout d'une classe
            addClass(self.reponses, "qcm-reponse")
            nrep=self.reponses.find_all("li")
            if len(nrep) != 4:
                self.addError(f"dans les réponses : il faut exactement 4 réponses, et on en trouve {len(rep)}.")
        if self.meta:
            metaPattern = re.compile(
                r".*ok[^\d]*(\d+).*niveau[^=]*=\s*([a-zA-Z]+).*",
                re.IGNORECASE|re.DOTALL,
            )
            m=metaPattern.match(str(self.meta))
            if not m:
                self.addError(f"dans les métadonnées : impossible de déterminer le numéro de réponse « OK » et le niveau de l'exercice.")
            else:
                self.ok=int(m.group(1))
                self.niveau=m.group(2)
        return

def cellContent(td):
    """
    récupère le texte d'une case de tableau, si celui-ci est dans
    un élément <p> ; simplifie les retours à la ligne, tabulations
    et espaces.
    @param td un objetc de type beautifulsoup, pour un élément <td>
    """
    result=td.find("p")
    if result != None:
        result=result.string
        if result != None:
            result=result.strip()
        else:
            result=""
    else:
        result=""
    return re.sub(r"[\n\t ]+", " ", result)

def addClass (tag, new):
    """
    ajout d'une classe, avec bs4
    @param tag  un objet beautifulsoup
    @param new une classe à ajouter
    """
    if not tag.has_attr('class'):
        tag['class']=new
    else:
        if new in tag['class']:
            pass
        else:
            tag['class']+= " "+new
    return

def encadre (message, largeur=76):
    """
    Renvoie un message texte encadré, taqué à gauche
    @param message une chaîne de caractères, éventuellement multiligne
    @param largeur la largeur intérieure du cadre
    """
    lignes=message.split("\n")
    resultat = [ "+"+"-"*largeur+"+"]
    for l in lignes:
        gauche = 1
        droite = largeur - len(l) - gauche
        resultat.append ("|"+" "*gauche+l+" "*droite+"|")
    resultat.append("+"+"-"*largeur+"+")
    return "\n".join(resultat)

if __name__=="__main__":
    try:
        odtFileName=sys.argv[1]
    except:
        print(encadre (usage.format(programme=sys.argv[0])))
        sys.exit(1)
        
    # création de sous-répertoires pour les résultats
    os.makedirs(odtFileName+".html", exist_ok=True) # pour l'export par soffice
    os.makedirs(odtFileName+".qcm", exist_ok=True)  # pour l'export par Python
    
    # conversion automatique par Writer
    cmd= f"""soffice --headless --convert-to html --outdir '{odtFileName+".html"}' '{odtFileName}'"""
    subprocess.call(cmd, shell=True)
    
    # recopie des media
    cmd=f"""cd {odtFileName+".html"}; rsync $(ls | grep -v 'html$') ../{odtFileName+".qcm"}"""
    subprocess.call(cmd, shell=True)

    # analyse et conversion du fichier html
    html_doc=open(os.path.join(odtFileName+".html", re.sub(r"\.odt$",".html",odtFileName))).read()
    soup = BeautifulSoup(html_doc, 'lxml')
    # on retire tous les styles des lignes et cases des tableaux
    for t in soup.find_all("table"):
        t["style"]="width: 100%;"
        for attr in ('cellpadding' 'cellspacing','width'):
            if t.has_attr(attr): del t[attr]
        for tr in t.find_all("tr"):
            if tr.has_attr('style'): del tr['style']
            for td in tr.find_all("td"):
                if td.has_attr('style'): del td['style']
    problems=soup.find_all("div",id=re.compile(r"^QCM_\d+$"))
    html=HEADER
    for p in problems:
        ident=re.match(r"QCM_(\d+)",p['id']).group(1)
        themes=p.find_all("td")
        qcm=QCM(
            soup,
            ident,
            cellContent(themes[0]),
            cellContent(themes[1]),
            cellContent(themes[2]),
            p.find("div", id="QCM_enonce_"+ident),
            p.find("div", id="QCM_question_"+ident), 
            p.find("div", id="QCM_meta_"+ident), 
        )
        html+=qcm.toReveal()
    html+=FOOTER
    outName=os.path.join(
        odtFileName+".qcm", re.sub(r"\.odt$",".html",odtFileName))
    with open(outName,"w") as outfile:
        outfile.write(html)
    print("On a créé", outName)
    
    # mise en place des bibliothèques javascript et CSS
    thisDir=os.path.dirname(__file__)
    cmd=f"""cp -Rd {thisDir}/aux '{odtFileName}.qcm'"""
    subprocess.call(cmd, shell=True)
    # À ce stade, on dispose d'une page web fonctionnelle
    print(f"""Le QCM à {odtFileName}.qcm/{re.sub(r".odt$",".html",odtFileName)} est actif.""")

    # création du paquet SCORM
    cmd=f"""rm -rf "{odtFileName}.scorm"; cp -Rd "{odtFileName}.qcm" "{odtFileName}.scorm" """
    subprocess.call(cmd, shell=True)

    # mis en place du fichier imsmanifest.xml
    cmd=f"""cp {thisDir}/imsmanifest.xml "{odtFileName}.scorm" """
    subprocess.call(cmd, shell=True)
          
    cmd=f"""cd "{odtFileName}.scorm"; mv {re.sub(r".odt$",".html",odtFileName)}  index.html; sed -i 's%<!-- scormfunctions -->%<script src="aux/scormfunctions.js"></script>%' index.html; zip -r {re.sub(r".odt$",".zip",odtFileName)} *"""
    subprocess.call(cmd, shell=True)
    
    print(f"""module scorm présent dans {odtFileName}.scorm/{re.sub(r".odt$",".zip",odtFileName)}""")
